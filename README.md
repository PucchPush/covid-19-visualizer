# COVID 19 Visualizer
This is a small script written in R 3.6.3 for visualizing the number of confirmed patients, recoveries, and deaths resulting from COVID 19 around the world.
I wrote this program to get a good look on the outbreak progress in Northern Europe so default the program will plot data for Finland, Denmark, Norway, and Sweden.
This is however configurable.<br>
The script uses data from the CSSE COVID-19 dataset available [here](https://github.com/CSSEGISandData/COVID-19)<br>

The program is built using RStudio on Windows 10. Bugs could exist on other platforms.

## How to set it up
1. Clone the project
2. Download the latest CSSE dataset from [here](https://github.com/CSSEGISandData/COVID-19)
3. Extract the CSSE dataset and copy it's master-folder to the root folder of this project. The path to the daily reports should look like `covid-19-visualizer\COVID-19-master\csse_covid_19_data\csse_covid_19_daily_reports`
4. Remove daily reports from before February 2nd 2020 by deleting their CSV files. This is because the column structure in those are different and so far the script is unable to parse them correctly. They will generate an rbind error `Error in rbind(deparse.level, ...) :
numbers of columns of arguments do not match`
5. Start up RStudio and open the corona.R file
6. Set the working directory to the source file location
7. That's it, you should now be able to run the program.

## Prediction
The visualizer contains a basic prediction model based on the [SIR model](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology#The_SIR_model)<br>
It should be noted that the SIR model is a very simple model which assumes constant transmission factors. It does not take into consideration stuff like
quarantine measures or population behaviour changes (like self isolating etc). **The prediction it will give is an absolute doomsday worst case scenario.**<br><br>
In order to activate prediction:
1. Add the country you want to predict for to *interesting_countries* (only one country can be predicted at at time)
2. Set *predict_country* to the country you want to predict
3. Set *country_population* to the country's current population
4. Set *prediction_length* to the amount of days forward you want to predict for.
5. Run the script, a vertical line will show on the graph where the predicted values start


## Known Issues

- `Error in rbind(deparse.level, ...) :numbers of columns of arguments do not match` : Remove the daily report csv files from before February 2nd 2020. This error is caused by them having a different column structure than the later reports.

## Resolved Issues
- ~~Plotting of countries where the total report is split up between regions (Mainland China, US, etc) will confuse the plotter and you will get a sawblade shape graph. This is a limitation in the script and one that is on the top of my list to fix. The cause of this error is that the regional reports can be quite small comared to the earlier national reports and the script does not know how to add the regional reports up yet. I ask that you are patient with this limitation for the time being.~~<br> Resolved with 35ed0ab9ba75ed830460d7da150ce132c07f96e9. If you see this issue, make sure you have an up-to-date version of the script. If your version is up-to-date, post an issue and I will address it.